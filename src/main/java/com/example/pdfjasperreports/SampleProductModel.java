package com.example.pdfjasperreports;

import lombok.Data;

@Data
public class SampleProductModel {

    private String product_name;
    private int price;

    public SampleProductModel(String name, int Price){
        this.product_name = name;
        this.price = Price;
    }
}
