package com.example.pdfjasperreports;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.SimpleJasperReportsContext;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.export.PdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    ResourceLoader resource;

    @GetMapping(value = "/sample")
    public void sample(HttpServletResponse response) {
        HashMap<String, Object> params = new HashMap<>();

        //ヘッダーデータ作成
        params.put("Client_name", "山本証券");
        params.put("Date_today", "平成30年5月1日");


        List<SampleProductModel> fields = new ArrayList<>();
        fields.add(new SampleProductModel("サンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルAサンプルA",1000));
        fields.add(new SampleProductModel("サンプルB",2000));
        fields.add(new SampleProductModel("サンプルC",3000));
        fields.add(new SampleProductModel("サンプルD",4000));
        fields.add(new SampleProductModel("サンプルE",5000));
        fields.add(new SampleProductModel("サンプルF",6000));
        fields.add(new SampleProductModel("サンプルG",7000));
        fields.add(new SampleProductModel("サンプルH",8000));
        fields.add(new SampleProductModel("サンプルI",9000));
        fields.add(new SampleProductModel("サンプルJ",10000));
        fields.add(new SampleProductModel("サンプルK",11000));
        fields.add(new SampleProductModel("サンプルL",12000));
        fields.add(new SampleProductModel("サンプルM",13000));
        fields.add(new SampleProductModel("サンプルN",14000));
        fields.add(new SampleProductModel("サンプルO",15000));
        fields.add(new SampleProductModel("サンプルP",16000));
        fields.add(new SampleProductModel("サンプルQ",17000));
        fields.add(new SampleProductModel("サンプルR",18000));
        fields.add(new SampleProductModel("サンプルS",19000));
        fields.add(new SampleProductModel("サンプルT",2000));
        fields.add(new SampleProductModel("サンプルV",2100));
        fields.add(new SampleProductModel("サンプルV",2200));
        fields.add(new SampleProductModel("サンプルW",2300));
        fields.add(new SampleProductModel("サンプルX",2400));
        fields.add(new SampleProductModel("サンプルY",2500));
        fields.add(new SampleProductModel("サンプルZ",2600));

        byte[] output  = OrderReporting2(params, fields);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + "sample.pdf");
        response.setContentLength(output.length);

        try (OutputStream os = response.getOutputStream()){
            os.write(output);
            os.flush();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    private byte[] OrderReporting2(HashMap<String, Object> param, List<SampleProductModel> data) {
        try (InputStream input = new FileInputStream(resource.getResource("classpath:jasper-report/Skillsheet.jrxml").getFile())) {
            //帳票ファイルを取得

            //リストをフィールドのデータソースに
            // 参考：https://stackoverflow.com/questions/22340535/how-to-show-jrbeancollectiondatasource-data-with-help-of-table-component
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
            param.put("DS1", dataSource);
            //帳票をコンパイル
            JasperReport jasperReport = JasperCompileManager.compileReport(input);

            JasperPrint jasperPrint;
            //パラメーターとフィールドデータを注入
            jasperPrint = JasperFillManager.fillReport(jasperReport, param, new JREmptyDataSource());
            jasperPrint.setProperty(PdfExporterConfiguration.PROPERTY_METADATA_TITLE, "Hogeeeee");

            //帳票をByte形式で出力
            return JasperExportManager.exportReportToPdf(jasperPrint);

        } catch (FileNotFoundException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (JRException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }
}
